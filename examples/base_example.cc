#include <iostream>

#include "../src/pnt2.h"
#include "../src/convex_hulls2.h"

int main()
{
    std::cout << "Here are some examples of usages\n" << std::endl;

    std::vector<Pnt2> vertices = { {(Real)0,  (Real)0 },
                                   {(Real)3,  (Real)1 },
                                   {(Real)4,  (Real)4 },
                                   {(Real)1,  (Real)3 } };


    std::cout << "Define simple polygon with vertices are:\n";
    for(auto& vertex : vertices)
    {
        std::cout << vertex;
    }

    std::cout << "\n";

    Polygon2 plgn(vertices);
    Real perimeter = plgn.perimeter();

    std::cout << "Perimeter of polygon is " << perimeter << "\n";

    std::cout << "Vertices of polygon are oriented ";
    if(plgn.checkOrientation())
        std::cout << "CounterClockWise.\n";
    else
        std::cout << "ClockWise.\n";

    std::cout << "Polygon is ";
    if(plgn.isConvex())
        std::cout << "convex.\n";
    else
        std::cout << "concave.\n";

    std::cout << "\n\n";

    std::vector<Pnt2> _test = { {(Real)0,  (Real)0 },
                                {(Real)0,  (Real)-5},
                                {(Real)2,  (Real)-1},
                                {(Real)1,  (Real)1 },
                                {(Real)3,  (Real)2 },
                                {(Real)-1, (Real)1 },
                                {(Real)-1, (Real)-1},
                                {(Real)-3, (Real)-2} };

    std::cout << "Let's define set of points:\n";
    for(auto& pnt : _test)
    {
        std::cout << pnt;
    }

    std::cout << "\n";

    std::cout << "Convex hull of given set of points, constructed by Graham scan:\n";

    ConvexHullGraham ghull(_test);
    for(auto& pnt : ghull.vertices_)
    {
        std::cout << pnt;
    }

    std::cout << "\n";

    std::cout << "Convex hull of given set of points, constructed by Jarvis March:\n";

    ConvexHullJarvis jhull(_test);
    for(auto& pnt : jhull.vertices_)
    {
        std::cout << pnt;
    }

    return 0;
}