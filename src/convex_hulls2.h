#pragma once

#include <cmath>

#include <algorithm>
#include <vector>
#include <deque>

#include "cg_math.h"
#include "pnt2.h"
#include "polygon2.h"


class ConvexHullGraham
{

public:

    ConvexHullGraham(const std::vector<Pnt2>& pnts)
    {
        init(pnts);
    }

    //to do: create getter for vertices_ and move it to private
    std::vector<Pnt2> vertices_;

private:

    class GPnt2 : public Pnt2
    {

    public:

        GPnt2(Real x, Real y ,Real theta) : theta_(theta)
        {
            x_ = x;
            y_ = y;
        };

        Real polarAngle(const Pnt2& origin) const
        {
            double theta = std::atan2(y_ - origin.y_, x_ - origin.x_);
            return ((theta < 0) ? theta += 2 * M_PI : theta);
        }

    private:

        Real theta_;
    };

    void getPivotPoint(std::vector<GPnt2>& pnts_);
    void angleSort    (std::vector<GPnt2>& pnts_);
    void grahamScan   (std::vector<GPnt2>& pnts_);

    void init(const std::vector<Pnt2>& pnts);

    struct OrdinateComparator
    {
        bool operator()(GPnt2 lhs, GPnt2 rhs) const
        {
            return ((lhs.y_ < rhs.y_) || (isEqual(lhs.y_, rhs.y_)) && (lhs.x_ < rhs.x_));
        }
    };

    struct AngleComparator
    {
        AngleComparator(GPnt2 origin) : origin_(origin) {};

        bool operator()(GPnt2 lhs, GPnt2 rhs) const
        {
            Real lhsTheta = lhs.polarAngle(origin_);
            Real rhsTheta = rhs.polarAngle(origin_);

            return ((lhsTheta < rhsTheta) || (isEqual(lhsTheta, rhsTheta) && (lhs.dist(origin_) < rhs.dist(origin_))));
        }

        GPnt2 origin_;
    };

};

class ConvexHullJarvis
{
public:

    ConvexHullJarvis(const std::vector<Pnt2>& pnts)
    {
        init(pnts);
    }

    Pnt2 getPivotPoint(      std::vector<Pnt2>& pnts );
    void angleSort    (      std::vector<Pnt2>& pnts_);
    void jarvisMarch  (      std::vector<Pnt2>& pnts_);
    void init         (const std::vector<Pnt2>& pnts );

    struct AbscissaCompararator
    {
        bool operator()(Pnt2 lhs, Pnt2 rhs) const
        {
            return ((lhs.x_ < rhs.x_) || (isEqual(lhs.x_, rhs.x_)) && (lhs.y_ < rhs.y_));
        }
    };

    struct AngleComparator
    {
        AngleComparator(Pnt2 origin) : origin_(origin) {};

        bool operator()(Pnt2 lhs, Pnt2 rhs) const
        {
            Real lhsTheta = lhs.polarAngle(origin_);
            Real rhsTheta = rhs.polarAngle(origin_);

            return ((lhsTheta < rhsTheta) || (isEqual(lhsTheta, rhsTheta) && (lhs.dist(origin_) < rhs.dist(origin_))));
        }

        Pnt2 origin_;
    };

    std::deque<Pnt2> vertices_;
};