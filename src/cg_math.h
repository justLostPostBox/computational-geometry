#pragma once

#include <cmath>
#include <limits>

typedef double Real;

const double eps = std::numeric_limits<Real>::epsilon();

template <typename Real>
inline bool isEqual(Real a, Real b)
{
    double diff = std::fabs(a - b);
    a = std::fabs(a);
    b = std::fabs(b);

    double min = (a > b) ? b : a;

    return (diff <= min * eps);
};
