#include "polygon2.h"


Real Polygon2::perimeter() const
{
    Real perimeter = (Real)0;

    std::vector<Pnt2>::const_iterator vCurr = vertices_.end() - 1;
    std::vector<Pnt2>::const_iterator vNext = vertices_.begin();

    while(vNext < vertices_.end())
    {
        perimeter += vCurr->dist(*vNext);
        vCurr = vNext;
        ++vNext;
    }

    return perimeter;
}

bool Polygon2::checkOrientation() const
{
    std::vector<Pnt2>::const_iterator vCurr = vertices_.end() - 1;
    std::vector<Pnt2>::const_iterator vNext = vertices_.begin();

    Real doubledSignedArea = (Real)0;

    while(vNext < vertices_.end())
    {
        doubledSignedArea += (vNext->x_ - vCurr->x_) * (vNext->y_ + vCurr->y_);
        vCurr = vNext;
        ++vNext;
    }

    return (doubledSignedArea < 0);
}

bool Polygon2::isConvex() const
{
    if(vertices_.empty())
        return false;
    if(vertices_.size() <= 3)
        return true;

    std::vector<Pnt2>::const_iterator vPrev = vertices_.end() - 2;
    std::vector<Pnt2>::const_iterator vCurr = vertices_.end() - 1;
    std::vector<Pnt2>::const_iterator vNext = vertices_.begin();

    while(vNext < vertices_.end())
    {
        if(!(oriented(*vPrev, *vCurr, *vNext) == Orientation::CounterClockwise) && isCCW_)
            return false;

        vPrev = vCurr;
        vCurr = vNext;
        ++vNext;
    }
    return true;
}

Real Polygon2::computeLength(size_t numV1, size_t numV2)
{
    //invalid query
    assert(   (numV1 < numV2)
              && (numV1 < vertices_.size())
              && (numV2 < vertices_.size()));

    Real length = (Real)0;

    while(numV1 < numV2)
    {
        length += vertices_.at(numV1).dist(vertices_.at(numV1+1));
        ++numV1;
    }

    return  length;
}
