#pragma once

#include <vector>
#include <cassert>

#include "pnt2.h"

struct Polygon2
{
    Polygon2() = default;

    Polygon2(std::vector<Pnt2>& vertices) : vertices_(vertices)
    {
        isCCW_ = checkOrientation();
    }

    Real perimeter() const;

    // return true if polygon oriented CCW, otherwise return false
    bool checkOrientation() const;

    bool isConvex() const;

    // compute length of border from vertex v1 with  to v2, note vertices are renumbered from 0
    Real computeLength(size_t numV1, size_t numV2);

    bool isCCW_;
    std::vector<Pnt2> vertices_;
};
