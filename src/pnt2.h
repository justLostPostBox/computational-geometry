#pragma once

#include <iostream>

#include "cg_math.h"


enum class Orientation {CounterClockwise, Clockwise, Collinear};

struct Pnt2
{
    Pnt2() : x_((Real)0.), y_((Real)0.) {};
    Pnt2(Real x, Real y) : x_(x), y_(y) {};

    Real dist(const Pnt2& other) const;
    Real polarAngle() const;
    Real polarAngle(const Pnt2& origin) const;
    bool operator==(const Pnt2& rhs) const;

    friend bool areEqual(Pnt2 lhs, Pnt2 rhs);
    friend Orientation oriented(Pnt2 pnt1, Pnt2 pnt2, Pnt2 pnt3);
    friend std::ostream& operator<< (std::ostream& os, Pnt2 pnt);

    double x_;
    double y_;
};

