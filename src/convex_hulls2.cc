#include "convex_hulls2.h"

void ConvexHullGraham::init(const std::vector<Pnt2>& pnts)
{
    std::vector<GPnt2> pnts_;

    for(auto& pnt : pnts)
    {
        pnts_.push_back(GPnt2(pnt.x_, pnt.y_, (Real)0));
    }

    getPivotPoint(pnts_);
    angleSort(pnts_);
    grahamScan(pnts_);
}

void ConvexHullGraham::getPivotPoint(std::vector<GPnt2>& pnts_)
{
    GPnt2 pivotPnt = *std::min_element(pnts_.begin(), pnts_.end(), OrdinateComparator());
    std::swap(pnts_.front(), pivotPnt);
}

void ConvexHullGraham::angleSort(std::vector<GPnt2>& pnts_)
{
    std::sort(pnts_.begin() + 1, pnts_.end(), AngleComparator(pnts_.front()));
}

void ConvexHullGraham::grahamScan(std::vector<GPnt2>& pnts_)
{
    std::deque<GPnt2> pntsQueue = {pnts_[0], pnts_[1]};

    std::vector<GPnt2>::const_iterator currPnt = (pnts_.begin() + 2);

    for(currPnt; currPnt < pnts_.end(); ++currPnt)
    {
        while(oriented(*pntsQueue.begin(), *(++pntsQueue.begin()), *currPnt) != Orientation::Clockwise)
        {
            pntsQueue.pop_front();
        }

        pntsQueue.push_front(*currPnt);
    }

    for(auto gPnt = pntsQueue.rbegin(); gPnt < pntsQueue.rend(); ++gPnt)
    {
        vertices_.push_back(Pnt2((*gPnt).x_, (*gPnt).y_));
    }
}

void ConvexHullJarvis::init(const std::vector<Pnt2>& pnts)

{

    std::vector<Pnt2> pnts_ = pnts;

    Pnt2 lowest_point = getPivotPoint(pnts_);
    vertices_.push_back(lowest_point);

    Pnt2 previous_point = lowest_point;
    Pnt2 current_point = *pnts_.begin();

    do
    {
        current_point = *pnts_.begin();

        for (auto it = pnts_.begin(); it != pnts_.end(); ++it)
        {
            if (oriented(previous_point, current_point, *it) == Orientation::CounterClockwise)
            {
                current_point = *it;
            }
        }

        if (!areEqual(current_point,lowest_point))
        {
            vertices_.push_front(current_point);
        }

        previous_point = current_point;
    }

    while (!areEqual(current_point,lowest_point));

}

Pnt2 ConvexHullJarvis::getPivotPoint(std::vector<Pnt2>& vertices_)
{
    return *std::min_element(vertices_.begin(), vertices_.end(), AbscissaCompararator());
}