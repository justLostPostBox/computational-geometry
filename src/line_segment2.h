#pragma once

#include <cmath>

#include "cg_math.h"
#include "pnt2.h"
#include "circle2.h"

enum class IntersectionResult {COINCIDENT, PARALLEL, INTERSEC, NOT_INTERSEC};

struct LineSegment2
{
    Pnt2 start_;
    Pnt2 end_;

    LineSegment2(                                  ) : start_(     ), end_(   ) {}
    LineSegment2(const Pnt2& start, const Pnt2& end) : start_(start), end_(end) {}

    Real length() const;
    bool intersectCircle(const Circle& crcl) const;

    //to do: change returning type from Real to bool, and add predicates LT to Pnt2 class
    friend Real bboxCheck                        (const LineSegment2& s1   , const LineSegment2& s2);
    friend IntersectionResult segmentIntersection(const LineSegment2& segmA, const LineSegment2& segmB,
                                                   Pnt2& intersection);
};