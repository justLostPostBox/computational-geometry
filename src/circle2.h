#pragma once


#include "cg_math.h"
#include "pnt2.h"

struct Circle
{
    Pnt2 center_;
    Real r_;

    Circle() : center_(), r_(1.){};
    Circle(Pnt2 center, Real r) : center_(center), r_(r) {};
};