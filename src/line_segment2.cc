#include "line_segment2.h"

Real LineSegment2::length() const
{
    return sqrt(pow(end_.x_ - start_.x_, 2) + pow(end_.y_ - start_.y_, 2));
}

//Real bboxCheck(const LineSegment2& s1, const LineSegment2& s2)
//{
//    return      std::max(s1.start_, s2.start_) <  std::min(s1.end_, s2.end_)
//             || std::max(s1.start_, s2.start_) == std::min(s1.end_, s2.end_);
//}

IntersectionResult segmentIntersection(const LineSegment2& segmA, const LineSegment2& segmB, Pnt2& intersection)
{
    Real denom   = ((segmB.end_.y_ - segmB.start_.y_) * (segmA.end_.x_ - segmA.start_.x_))
                   -
                   ((segmB.end_.x_ - segmB.start_.x_) * (segmA.end_.y_ - segmA.start_.y_));

    Real numer_a = ((segmB.end_.x_ - segmB.start_.x_) * (segmA.start_.y_ - segmB.start_.y_))
                   -
                   ((segmB.end_.y_ - segmB.start_.y_) * (segmA.start_.x_ - segmB.start_.x_));

    Real numer_b = ((segmA.end_.x_ - segmA.start_.x_) * (segmA.start_.y_ - segmB.start_.y_))
                   -
                   ((segmA.end_.y_ - segmA.start_.y_) * (segmA.start_.x_ - segmB.start_.x_));

    bool bboxResult = bboxCheck(segmA, segmB);

    if(isEqual(denom, (Real)0))
    {
        if(   isEqual(numer_a, (Real)0) && isEqual(numer_b, (Real)0)
           && bboxResult)
            return IntersectionResult::COINCIDENT;

        return IntersectionResult::PARALLEL;
    }

    Real t_a = numer_a / denom;
    Real t_b = numer_b / denom;

    if (   t_a >= 0.0 && t_a <= 1.0
        && t_b >= 0.0 && t_b <= 1.0)
    {
        intersection.x_ = segmA.start_.x_ + t_a *(segmA.end_.x_ - segmA.start_.x_);
        intersection.y_ = segmA.start_.y_ + t_a *(segmA.end_.y_ - segmA.start_.y_);

        return IntersectionResult::INTERSEC;
    }

    return IntersectionResult::NOT_INTERSEC;
}

bool LineSegment2::intersectCircle(const Circle& crcl) const
{
    //at^2+bt+c=0 where:
    double a, b, c;

    a = pow((end_.x_ - start_.x_),2) + pow((end_.y_ - start_.y_),2);

    b = 2 * ((start_.x_ - crcl.center_.x_) * (end_.x_ - start_.x_) +
             (start_.y_ - crcl.center_.y_) * (end_.y_ - start_.y_));

    c = pow((start_.x_ - crcl.center_.x_),2) + pow((start_.y_ - crcl.center_.y_),2) - pow(crcl.r_,2);

    double discr = pow(b,2) - 4*a*c;

    double t1 = (-b + sqrt(discr))/(2*a);
    double t2 = (-b - sqrt(discr))/(2*a);

    return ((isEqual(t1, t2) && t1 > 0. && t1 < 1.)
            ||((t1 > 0. && t1 < 1.) && (t2 > 0. && t2 < 1.) || isEqual(t1, 0.) || isEqual(t2, 0.) || isEqual(t1, 1.) || isEqual(t2, 1.))
            ||((t1 > 0. && t1 < 1.) || (t2 > 0. && t2 < 1.) || isEqual(t1, 0.) || isEqual(t2, 0.) || isEqual(t1, 1.) || isEqual(t2, 1.)));
}