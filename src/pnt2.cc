#include "pnt2.h"

Real Pnt2::dist(const Pnt2& other) const
{
    Real dx = other.x_ - x_;
    Real dy = other.y_ - y_;

    return sqrt(dx*dx + dy*dy);
}

Real Pnt2::polarAngle() const
{
    double theta = atan2(y_, x_);
    return ((theta < 0) ? theta += 2 * M_PI : theta);
}

Real Pnt2::polarAngle(const Pnt2& origin) const
{
    double theta = atan2(y_ - origin.y_, x_ - origin.x_);
    return ((theta < 0) ? theta += 2 * M_PI : theta);
}

Orientation oriented(Pnt2 pnt1, Pnt2 pnt2, Pnt2 pnt3)
{
    Real term = ((pnt1.x_ - pnt3.x_) * (pnt2.y_ - pnt3.y_)) - ((pnt2.x_ - pnt3.x_) * (pnt1.y_ - pnt3.y_));

    if (term > (Real)0)      return Orientation::CounterClockwise;
    else if (term < (Real)0) return Orientation::Clockwise;
    else                     return Orientation::Collinear;
}

std::ostream& operator<<(std::ostream& os, Pnt2 pnt)
{
    return os << "(" << pnt.x_ << ", " << pnt.y_ << ")";
}

bool areEqual(Pnt2 lhs, Pnt2 rhs)
{
    return     isEqual(lhs.x_, rhs.x_)
            && isEqual(lhs.y_, rhs.y_);
}


bool Pnt2::operator==(const Pnt2& rhs) const
{
    return areEqual(*this, rhs);
}
