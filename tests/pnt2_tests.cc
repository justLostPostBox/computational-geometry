#include <gtest/gtest.h>

#include "../src/pnt2.h"

TEST(Pnt2BaseTests, CheckDistanceBetweenPnts)
{
    Pnt2 pnt    ((Real)-5, (Real)12);
    Pnt2 origin ((Real)0 , (Real)0 );

    EXPECT_EQ(13, origin.dist(pnt));
}

TEST(Pnt2BaseTests, CheckPolarAngleFunction)
{
    Pnt2 pnt1((Real)0, (Real)1  );
    Pnt2 pnt2((Real)0, (Real)100);

    EXPECT_EQ(pnt1.polarAngle(), pnt2.polarAngle());
    EXPECT_EQ(M_PI_2, pnt1.polarAngle());

    //At definition Pnt2::polarAngle() using atan2, which range is (-PI/2, PI/2), but our goal is to produce results
    // in the range [0, 2PI) We archive this by just adding 2PI if result of atan2 is negative. Check it here:
    Pnt2 pnt3((Real)0, (Real)-1  );
    Pnt2 pnt4((Real)0, (Real)-100);

    EXPECT_EQ(pnt3.polarAngle(), pnt4.polarAngle());
    EXPECT_EQ((-M_PI_2 + 2 * M_PI), pnt3.polarAngle());
}

TEST(Pnt2BaseTests, CheckPolarAngleFunctionWithMutableOrigin)
{
    Pnt2 A        ((Real)100, (Real)100);
    Pnt2 origin   ((Real)0  , (Real)0  );
    Pnt2 newOrigin((Real)1  , (Real)1  );

    //Assume theta is a polar angle of some point A. Expected that theta doesn't changed, if we move origin to the point
    // lying at the same line between A and default origin.
    EXPECT_EQ(A.polarAngle(), A.polarAngle(newOrigin));
}

TEST(Pnt2BaseTests, CheckEqualityPredicate)
{
    Pnt2 pnt1( (Real)0       ,  (Real)0      );
    Pnt2 pnt2(((Real)0) + eps, ((Real)0) +eps);

    EXPECT_FALSE(areEqual(pnt1, pnt2));

    Pnt2 pnt3(std::numeric_limits<Real>::lowest(), std::numeric_limits<Real>::max()   );
    Pnt2 pnt4(std::numeric_limits<Real>::max()   , std::numeric_limits<Real>::lowest());

    EXPECT_FALSE(areEqual(pnt3, pnt4));

    Pnt2 pnt5(std::numeric_limits<Real>::lowest(), std::numeric_limits<Real>::lowest());
    Pnt2 pnt6(std::numeric_limits<Real>::lowest(), std::numeric_limits<Real>::lowest());

    EXPECT_TRUE(areEqual(pnt5, pnt6));
    EXPECT_TRUE(areEqual(pnt6, pnt5));
}

TEST(Pnt2BaseTests, CheckOrintationPredicate)
{
    //Collinear points
    Pnt2 pnt1((Real)0  , (Real)0  );
    Pnt2 pnt2((Real)1  , (Real)1  );
    Pnt2 pnt3((Real)100, (Real)100);

    EXPECT_TRUE(oriented(pnt1, pnt2, pnt3) == Orientation::Collinear);

    //CCW
    Pnt2 pnt4((Real)0, (Real)0);
    Pnt2 pnt5((Real)1, (Real)1);
    Pnt2 pnt6((Real)1, (Real)2);

    EXPECT_TRUE(oriented(pnt4, pnt5, pnt6) == Orientation::CounterClockwise);

    //CW
    Pnt2 pnt7((Real)0, (Real)0);
    Pnt2 pnt8((Real)1, (Real)1);
    Pnt2 pnt9((Real)1, (Real)0);

    EXPECT_TRUE(oriented(pnt7, pnt8, pnt9) == Orientation::Clockwise);

}