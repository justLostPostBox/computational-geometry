#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "../src/polygon2.h"

using ::testing::ContainerEq;

TEST(Polygon2BaseTests, CheckPerimeterFunction)
{
    std::vector<Pnt2> vertices = {{(Real)0, (Real)0},
                                  {(Real)1, (Real)0},
                                  {(Real)1, (Real)1},
                                  {(Real)0, (Real)1}};

    Polygon2 plgn(vertices);
    ASSERT_THAT(plgn.vertices_, ContainerEq(vertices));

    Real perimeter = plgn.perimeter();
    EXPECT_EQ((Real)4, perimeter);
}

